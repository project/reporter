<?php

declare(strict_types=1);

namespace Drupal\reporter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Reporter routes.
 */
final class ReporterReportController extends ControllerBase {

  /**
   * The controller constructor.
   */
  public function __construct(
    private readonly Connection $connection,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('database'),
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(): array {
    // Variables.
    $qty_results = 0;
    $reportName = '';
    $reportMachineName = '';
    $reportDescription = '';
    $reportHeader = [];
    $reportQuantityResults = '';

    // Show a Report or Menu page.
    if (is_array($_GET) && count($_GET)) {
      // Arguments are present this is a report.
      $rows = $this->getReportRows($reportName,
        $reportMachineName,
        $reportDescription,
        $reportHeader,
        $reportQuantityResults);
    }
    else {
      // No Arguments are present this is a menu.
      $rows = $this->getMenuRows($reportName,
        $reportMachineName,
        $reportDescription,
        $reportHeader,
        $reportQuantityResults);
    }

    // Output.
    $build['header'] = [
      '#type' => 'item',
      '#markup' => '<h3>' . $reportName . '</h3>',
    ];

    $build['pre-table'] = [
      '#children' => "<p>$reportDescription</p>",
    ];

    $build['content'] = [
      '#type' => 'item',
      '#markup' => "<h6>$reportQuantityResults Rows</h6>",
    ];

    $build['report_table'] = [
      '#type' => 'table',
      '#header' => $reportHeader,
      '#rows' => $rows,
      '#empty' => $this->t('No Results.'),
    ];

    return $build;

  }

  /**
   * Formats the menu Link.
   */
  public function formatMenuLink($reportName, $reportMachineName) {
    $arguments = [
      'reporter_machine_name' => $reportMachineName,
    ];
    $query = http_build_query($arguments);
    $title = 'Click To View Report In A New Window.';
    $link = "<a href=\"/admin/reports/reporter/report?$query\" title=\"$title\" target=\"_blank\">$reportName</a>";
    return ['data' => ['#markup' => $link]];
  }

  /**
   * Formats the agile Link.
   *
   * Includes Arguments from query.
   * If $thisColNum = $desiredColNum it produces a Link.
   * If $thisColNum != $desiredColNum it produces the Textual Data.
   * The linkName is name of the Link or the Data in the Column.
   * The $thisColNum argument is the column number in the table.
   * The $desiredColNum is thec column number that should be a link.
   * $queryStringArray is the row data for the Non Visible columns.
   * Remember you need to add 'reporter_machine_name' to your Select.
   */
  public function formatAgileLink($linkName, $queryStringArray, $reportConfigData) {
    $title = 'Click To View Detail Report In A New Window.';
    $report_link_url = ($reportConfigData['report_link_url'] ?? '');
    $report_link_url_argument_style = ($reportConfigData['report_link_url_argument_style'] ?? 'append_query_string');

    if ($report_link_url_argument_style == 'substitute_values_for_names') {
      // Substitute the invisible columns in the url.
      foreach ($queryStringArray as $name => $value) {
        $report_link_url = str_replace($name, $value, $report_link_url);
      }
      $link = "<a href=\"$report_link_url\" title=\"$title\" target=\"_blank\">$linkName</a>";
    }
    else {
      // Add the invisible columns as a Query String.
      // Build the Query String from the $queryStringArray.
      $query = http_build_query(array_merge($queryStringArray));

      // If the $report_link_url contains a question mark ?.
      if (str_contains($report_link_url, '?')) {
        $last_character_of_url = substr($report_link_url, -1);

        if ($last_character_of_url == '&' || $last_character_of_url == '?') {
          // Perfect just add the query.
          $link_url = "$report_link_url$query";
        }
        else {
          $link_url = "$report_link_url&$query";
        }
      }
      else {
        // No ? found. Add one before the query.
        $link_url = "$report_link_url?$query";
      }

      $link = "<a href=\"$link_url\" title=\"$title\" target=\"_blank\">$linkName</a>";
    }

    return ['data' => ['#markup' => $link]];
  }

  /**
   * Gets the report configuration for the $reportMachineName report.
   */
  public function getReportConfiguration($reportMachineName) : array {
    // Get The Report Data.
    $reportsConfig = \Drupal::config('reporter.settings')->get('reports');

    if (is_array($reportsConfig) === FALSE || count($reportsConfig) == 0) {
      return [];
    }

    foreach ($reportsConfig as $reportConfig) {
      if ($reportConfig['fieldset']['report_machine_name'] == $reportMachineName) {
        return $reportConfig['fieldset'];
      }
    }

    return [];
  }

  /**
   * Gets the report data.
   *
   * Massages the Query and returns the rows.
   */
  public function getReportData($reportSql, $numberOfVisibleColumns, $reportLinkColumn, $reportConfigData, &$reportHeader): array {
    // Check the SQL for undesired stuff.
    if (stripos($reportSql, 'INSERT') !== FALSE ||
        stripos($reportSql, 'UPDATE') !== FALSE ||
        stripos($reportSql, 'CALL') !== FALSE ||
        strpos($reportSql, 'users_field_data') > 0 && str_contains($reportSql, 'pass') == TRUE
        ) {

      return ['The Query contains INSERT, UPDATE, CALL or pass Only SELECT Is allowed.'];
    }

    // Prepare Substitutions.
    // Really would to know what to clean out of the Query String.
    // Besides report_machine_name.
    // But since you have to put them in there its sparse.
    $urlQueryStringArray = (is_array($_GET) && !empty($_GET) ? $_GET : '');

    if (is_array($urlQueryStringArray) && count($urlQueryStringArray)) {
      $substitution_names = [];
      $substitution_values = [];
      foreach ($urlQueryStringArray as $name => $value) {
        $substitution_names[] = $name;
        $substitution_values[] = $value;
      }

      // Substitute the names in the SQL with values.
      $massagedSql = str_replace($substitution_names, $substitution_values, $reportSql);

    }

    $rows = [];
    $linkArguments = [];

    // Add message with the SQL in it.
    if ($_GET['debug'] || $_GET['DEBUG']) {
      if (is_array($urlQueryStringArray) && count($urlQueryStringArray) > 0) {
        \Drupal::messenger()->addMessage($massagedSql);
      }
      else {
        \Drupal::messenger()->addMessage($reportSql);
      }
    }

    try {
      // Submit the Query to the database.
      $database = \Drupal::database();

      if (is_array($urlQueryStringArray) && count($urlQueryStringArray) > 0) {
        $results = $database->query($massagedSql);
      }
      else {
        $results = $database->query($reportSql);
      }

    }

    catch (\Exception $e) {

      $number_of_substitutions = (is_array($urlQueryStringArray) && count($urlQueryStringArray) ? count($urlQueryStringArray) : 0);

      // Put Errors in the log.
      if ($number_of_substitutions) {
        \Drupal::logger('reporter1')->notice(print_r($urlQueryStringArray, TRUE));
      }

      \Drupal::logger('reporter2')->notice(print_r($reportSql, TRUE));

      if ($number_of_substitutions) {
        \Drupal::logger('reporter3')->notice(print_r($massagedSql, TRUE));
      }

      \Drupal::logger('reporter4')->notice($e->getMessage());

      // Put Errors on the screen.
      return [
        ['Error Message:'], [$e->getMessage()],
        ['SQL:'], [$reportSql],
        ['Massaged SQL:'], [$reportSql],
        ['Number of Substitutions:'], [$number_of_substitutions],
        ['Substitutions:'], [print_r($substitutionArray, TRUE)],
      ];

    }

    if ($results) {
      while ($row = $results->fetchAssoc()) {
        // We have a row of data.
        $columnNumber = 0;
        $columnData = [];
        $reportHeader = [];
        $linkArguments = [];
        foreach ($row as $columnName => $columnValue) {
          $columnNumber++;
          if ($numberOfVisibleColumns == 0 || $columnNumber <= $numberOfVisibleColumns) {
            // Visible column in table.
            // Data.
            $columnData[] = $columnValue;

            // Header.
            $reportHeader[] = $columnName;

          }
          else {
            // Invisible query arg column.
            $linkArguments[$columnName] = $columnValue;
          }

        } // of for each column.

        // Now that all the Visible and columns have been read.
        // All the columns are available for calling formatAgileLink().
        if ($reportLinkColumn == 0) {
          $rows[] = $columnData;
        }
        else {
          $columnDataWithLink = [];
          foreach ($columnData as $index => $columnItem) {
            if ($index == ($reportLinkColumn - 1)) {
              // The Linked Column.
              $columnDataWithLink[] = $this->formatAgileLink($columnItem, $linkArguments, $reportConfigData);
            }
            else {
              // Regular Column.
              $columnDataWithLink[] = $columnItem;
            }
          }

          $rows[] = $columnDataWithLink;
        }

      }

      return $rows;

    }

  }

  /**
   * Builds the menu.
   */
  public function getMenuRows(
    &$reportName,
    &$reportMachineName,
    &$reportDescription,
    &$reportHeader,
    &$reportQuantityResults,
  ): array {

    $rows = [];

    $reports = \Drupal::config('reporter.settings')->get('reports');

    $reportDescription = 'List of available reports. Click on one to view it.';

    $reportQuantityResults = count($reports);

    // Iterate thru the reports.
    foreach ($reports as $report) {
      if ($report['fieldset']['hide_in_menu'] != TRUE) {
        $rows[] = [$this->formatMenuLink(
          $report['fieldset']['report_name'], $report['fieldset']['report_machine_name'] ?? ''),
          filter_var(strip_tags(html_entity_decode($report['fieldset']['report_description']['value'] ?? '')), FILTER_SANITIZE_STRING),
        ];
      }

    }

    return $rows;
  }

  /**
   * Builds the response.
   */
  public function getReportRows(
    &$reportName,
    &$reportMachineName,
    &$reportDescription,
    &$reportHeader,
    &$reportQuantityResults,
  ): array {

    // Get the specified reporter_machine_name from the URL query string.
    $reportMachineName = (isset($_GET['reporter_machine_name']) && !empty($_GET['reporter_machine_name']) ? $_GET['reporter_machine_name'] : '');

    // Get the rest of the specified report configuration.
    $reportConfigData = $this->getReportConfiguration($reportMachineName);

    // Set the report info.
    $reportName = $reportConfigData['report_name'];
    $reportDescription = $reportConfigData['report_description']['value'];

    $numberOfVisibleColumns = $reportConfigData['visible_column_count'];

    $reportLinkColumn = (int) $reportConfigData['link_column'];
    $reportSql = $reportConfigData['report_sql'];

    if (!empty($reportSql)) {
      // Get the report data from the database.
      $reportData = $this->getReportData($reportSql, $numberOfVisibleColumns, $reportLinkColumn, $reportConfigData, $reportHeader);
    }
    else {
      $reportData = [];
    }

    // Known after the query executes.
    $reportQuantityResults = count($reportData);

    return $reportData;
  }

}
