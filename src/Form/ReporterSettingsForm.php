<?php

declare(strict_types=1);

namespace Drupal\reporter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Reporter settings for this site.
 */
final class ReporterSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'reporter_reporter_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['reporter.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $reports_config_data = $this->config('reporter.settings')->get('reports');

    $form['#tree'] = TRUE;

    $form['description'] = [
      '#type' => 'item',
      '#title' => $this->t('Edit a report or Add another report.'),
    ];

    // Gather the number of types in the form already.
    $num_reports = $form_state->get('num_reports');

    // We have to ensure that there is at least one name field.
    if ($num_reports === NULL) {

      $this->loadRenderInfo($form_state);
      // Now you can set $num_reports.
      $num_reports = $form_state->get('num_reports');

    }

    // Build the number of 'type' fieldsets $form_state['num_types'].
    for ($report_index = 1; $report_index <= $num_reports; $report_index++) {

      // THE Fieldset.
      $form['reports'][$report_index]['fieldset'] = [
        '#type' => 'fieldset',
        '#title' => 'Report ' . $report_index,
      ];

      // In fieldset - Report Name.
      $form['reports'][$report_index]['fieldset']['report_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#required' => TRUE,
        '#default_value' => ($reports_config_data[$report_index]['fieldset']['report_name'] ?? ''),
      ];

      // In fieldset - Report Machine Name.
      $form['reports'][$report_index]['fieldset']['report_machine_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Machine Name'),
        '#description' => 'Use lowercase letters or numbers or an underscore. This is the name of the report used in urls. If you leave this blank one will be created for you.',
        '#default_value' => ($reports_config_data[$report_index]['fieldset']['report_machine_name'] ?? "report_$report_index"),
        '#required' => TRUE,
      ];

      // In fieldset - Report Description.
      $form['reports'][$report_index]['fieldset']['report_description'] = [
        '#type' => 'text_format',
        '#title' => $this->t('Description'),
        '#description' => 'Briefly describe the report. (optional)',
        '#format' => 'full_html',
        '#default_value' => ($reports_config_data[$report_index]['fieldset']['report_description']['value'] ?? ''),
      ];

      // In fieldset - Report SQL.
      $form['reports'][$report_index]['fieldset']['report_sql'] = [
        '#type' => 'textarea',
        '#title' => $this->t('SQL'),
        '#wysiwyg' => FALSE,
        '#description' => 'The SQL SELECT Query to produce the desired report.',
        '#default_value' => ($reports_config_data[$report_index]['fieldset']['report_sql'] ?? ''),
      ];

      // In fieldset - Quantity of Visible Columns.
      $form['reports'][$report_index]['fieldset']['visible_column_count'] = [
        '#title' => t('Quantity of Visible Columns'),
        '#type' => 'select',
        '#description' => 'Limit the number of Visible columns. Invisible columns appear as arguments in the query string of the Link column. They are used for Detail Pages (optional)',
        '#default_value' => ($reports_config_data[$report_index]['fieldset']['visible_column_count'] ?? 0),
        '#options' => [
          0 => 'All',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
          7 => '7',
          8 => '8',
          9 => '9',
          10 => '10',
          11 => '11',
          12 => '12',
          13 => '13',
          14 => '14',
          15 => '15',
          16 => '16',
          17 => '17',
          18 => '18',
          19 => '19',
          20 => '20',
        ],
      ];

      // In fieldset - Link Column.
      $form['reports'][$report_index]['fieldset']['link_column'] = [
        '#title' => t('Link Column'),
        '#type' => 'select',
        '#description' => 'Select which column will have a link. To use this you need to SELECT the columns you want to be included in the query string of the link in your SQL. (optional)',
        '#default_value' => ($reports_config_data[$report_index]['fieldset']['link_column'] ?? ''),
        '#options' => [
          0 => 'None',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
          7 => '7',
          8 => '8',
          9 => '9',
          10 => '10',
          11 => '11',
          12 => '12',
          13 => '13',
          14 => '14',
          15 => '15',
          16 => '16',
          17 => '17',
          18 => '18',
          19 => '19',
          20 => '20',
        ],
      ];

      // In fieldset - Link URL.
      $form['reports'][$report_index]['fieldset']['report_link_url'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Link URL'),
        '#size' => 100,
        '#description' => 'This is the url for the Link Column /admin/reports/reporter/report is the default path for Reporter Reports.',
        '#default_value' => ($reports_config_data[$report_index]['fieldset']['report_link_url'] ?? '/admin/reports/reporter/report'),

      ];

      // In fieldset - Link URL Style Options.
      $radioOptions = [
        'append_query_string' => t('Add Invisible Column Names and Values To URL Query String. (Default)'),
        'substitute_values_for_names' => t('Substitute Invisible Column Values For Names In The Link URL.'),
      ];

      // In fieldset - Link URL Style.
      $form['reports'][$report_index]['fieldset']['report_link_url_argument_style'] = [
        '#type' => 'radios',
        '#title' => t('How To Add Invisible Column Data To The Link URL'),
        '#options' => $radioOptions,
        '#description' => t('Choose which style of URL will be in the Link Column ?'),
        '#default_value' => ($reports_config_data[$report_index]['fieldset']['report_link_url_argument_style'] ?? 'append_query_string'),
      ];

      $form['reports'][$report_index]['fieldset']['hide_in_menu'] = [
        '#type' => 'checkbox',
        '#title' => t('Exclude this report from the Menu Page.'),
        '#description' => 'Exclude this report from the Menu Page. This would be done for Detail pages that need query arguments.',
        '#default_value' => ($reports_config_data[$report_index]['fieldset']['hide_in_menu'] ?? FALSE),
      ];

    }

    // Adds "Add another report" button.
    $form['add_type'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add another report'),
      '#submit' => [[$this, 'addReport']],
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {

    $values = ($form_state->getValues()['reports'] ?? []);

    $machineNameArray = [];

    foreach ($values as $report) {
      $machineNameArray[] = $report['fieldset']['report_machine_name'];

      $reportSql = $report['fieldset']['report_sql'];

      // Check the SQL for undesired stuff.
      if (stripos($reportSql, 'INSERT') !== FALSE ||
          stripos($reportSql, 'UPDATE') !== FALSE ||
          stripos($reportSql, 'CALL') !== FALSE ||
          strpos($reportSql, 'users_field_data') > 0 && str_contains($reportSql, 'pass') == TRUE
         ) {

        $form_state->setErrorByName(
          'message',
          $this->t('The Query contains INSERT, UPDATE, CALL or pass Only SELECT Is allowed.'),
        );

      }

      if (stripos($reportSql, ';') !== FALSE) {
        $form_state->setErrorByName(
          'message',
          $this->t('The SQL Query contains a ; (semi colon). ; are not allowed.'),
        );
      }

      // Check The machine-readable name must contain only.
      // Lowercase letters, numbers, and hyphens.
      $validity = $this->validMachineName($report['fieldset']['report_machine_name']);
      if ($validity === FALSE) {
        $form_state->setErrorByName(
          'message',
          $this->t('The Machine Names on all the reports must contain only lowercase letters, numbers, and hyphens.'),
        );
      }

    }

    // Make sure the machine names are unique.
    if (count($machineNameArray) != count(array_unique($machineNameArray))) {
      $form_state->setErrorByName(
        'message',
        $this->t('The Machine Names on all the reports must be different.'),
      );
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Get the form data.
    $values = ($form_state->getValues()['reports'] ?? []);

    $config = $this->config('reporter.settings');

    // Save the config data. Pretty easy.
    $config->set('reports', $values)->save();

    $form_state->setRebuild(TRUE);

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function addReport(array &$form, FormStateInterface $form_state): void {
    // Add another type.
    $num_reports = $form_state->get('num_reports');
    $form_state->set('num_reports', $num_reports + 1);

    // Set rebuild.
    $form_state->setRebuild(TRUE);
  }

  /**
   * Gets the number of types and rules in the config to render.
   */
  public function loadRenderInfo(FormStateInterface $form_state) {

    // Get the config data.
    $reports = ($this->config('reporter.settings')->get('reports') ?? []);

    // Set the number of Types.
    $form_state->set('num_reports', count($reports));

  }

  /**
   * Checks for valid machine names.
   */
  public function validMachineName($potential_machine_name) {

    $characters = str_split($potential_machine_name);
    foreach ($characters as $character) {
      $legal = $this->legalCharacter($character);
      if ($legal == FALSE) {
        return FALSE;
      }

    }

    return TRUE;

  }

  /**
   * Tests a character in the name to make sure its valid.
   */
  public function legalCharacter($character) {
    switch ($character) {
      case 'a':
      case 'b':
      case 'c':
      case 'd':
      case 'e':
      case 'f':
      case 'g':
      case 'h':
      case 'i':
      case 'j':
      case 'k':
      case 'l':
      case 'm':
      case 'n':
      case 'o':
      case 'p':
      case 'q':
      case 'r':
      case 's':
      case 't':
      case 'u':
      case 'v':
      case 'w':
      case 'x':
      case 'y':
      case 'z':
      case '_':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
      case '0':
        return TRUE;

      default:
        return FALSE;
    }

  }

}
